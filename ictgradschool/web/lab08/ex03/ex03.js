"use strict";

// Provided variables
var string1 = "Hello World";
var string2 = "Hi everybody!"
var string3 = "Hi, Dr Nick!"

// TODO Your answers here.

var stringLength = string1.length;
console.log("stringLength = " + stringLength);

var subString = string1.substring(8, 11);
console.log("lastLetters = " + subString);

var stringIndex = string2.indexOf(" ");
console.log("howManySpaces = " + stringIndex)

var simpleChar = string3.charAt(string3.length - 1);
console.log("lastLetter = " + simpleChar);